import { pageScroll } from '@egml/utils';
import * as panel from './panel.js';
import config from './config.js';
import template from './html/editable-area.js';
 
// Набор цветов для генерации разных классов стилей
var colors = [
	'red',
	'blue',
	'orange',
	'green',
	'violet',
];
 
// Список всех созданных областей
export var areas = [];
 
// Индикатор видимости областей
var shown = false;
 
class Area {
	constructor(options) {
		// FIXME: Добавить индикатор области при выводе ошибки, чтобы понимать с какой областью проблема
		if (!options.selector) {
			return console.error('EditableArea: не определен селектор');
		}
		if (!options.url) {
			return console.error('EditableArea: не определен URL');
		}
		this.dom = {};
		this.dom.target = document.querySelector(options.selector);
		if (!this.dom.target) {
			return console.error('EditableArea: не найден элемент в DOM');
		}
		 
		// Интервал обновления позиции и размера области
		this.repositionInterval = 1000;
		 
		// HTML
		var templateData = {
			parentFontSize: config.rootFontSize,
			hasTitle: options.title ? true : false,
			title: options.title,
		};
		var fragment = document.createRange().createContextualFragment(template(templateData));
		this.dom.self = fragment.firstElementChild;
		document.body.appendChild(fragment);
		 
		this.dom.self.addEventListener('click', function() {
			config.global.modal.show({
				url: options.url,
				callback: function() {
					panel.hide();
					toggle();
				},
			});
		});
		 
		// Подстройка позиции и размеров области
		this.offset = {
			left: 0,
			top: 0,
			right: 0,
			bottom: 0,
		};
		if (options.offset) {
			let matches = options.offset.match(/(-?\d+)(?: (-?\d+)(?: (-?\d+)(?: (-?\d+))?)?)?/);
			let o1 = matches[1];
			let o2 = matches[2];
			let o3 = matches[3];
			let o4 = matches[4];
			if (o1 && !o2 && !o3 && !o4) {
				this.offset.left =
				this.offset.top =
				this.offset.right =
				this.offset.bottom = Number(o1);
				// Можно еще так, но это только для целых чисел
				// this.offset.bottom = o1>>0;
			} else if (o1 && o2 && !o3 && !o4) {
				this.offset.top =
				this.offset.bottom = Number(o1);
				this.offset.left =
				this.offset.right = Number(o2);
			} else if (o1 && o2 && o3 && !o4) {
				this.offset.top = Number(o1);
				this.offset.left =
				this.offset.right = Number(o2);
				this.offset.bottom = Number(o3);
			} else if (o1 && o2 && o3 && o4) {
				this.offset.top = Number(o1);
				this.offset.right = Number(o2);
				this.offset.bottom = Number(o3);
				this.offset.left = Number(o4);
			}
		}
		 
		// Регистрация созданной области
		areas.push(this);
	}
	 
	show() {
		this.dom.self.classList.remove('egml-admin_layer-editable_area--hidden');
		this.position();
		this.interval = window.setInterval(this.position.bind(this), this.repositionInterval);
	}
	 
	hide() {
		this.dom.self.classList.add('egml-admin_layer-editable_area--hidden');
		window.clearInterval(this.interval);
		this.dom.self.style = null;
	}
	 
	position() {
		var rect = this.dom.target.getBoundingClientRect();
		var scroll = pageScroll();
		this.dom.self.style.left = rect.left - 5 + scroll.left - this.offset.left + 'px';
		this.dom.self.style.top = rect.top - 5 + scroll.top - this.offset.top + 'px';
		this.dom.self.style.width = rect.width + 10 + this.offset.left + this.offset.right + 'px';
		this.dom.self.style.height = rect.height + 10 + this.offset.top + this.offset.bottom + 'px';
	}
}
 
//	 ██████ ██████  ███████  █████  ████████ ███████
//	██      ██   ██ ██      ██   ██    ██    ██
//	██      ██████  █████   ███████    ██    █████
//	██      ██   ██ ██      ██   ██    ██    ██
//	 ██████ ██   ██ ███████ ██   ██    ██    ███████
//
//	#create
export function create(options) {
	new Area(options);
}
 
//	████████  ██████   ██████   ██████  ██      ███████
//	   ██    ██    ██ ██       ██       ██      ██
//	   ██    ██    ██ ██   ███ ██   ███ ██      █████
//	   ██    ██    ██ ██    ██ ██    ██ ██      ██
//	   ██     ██████   ██████   ██████  ███████ ███████
//
//	#toggle
export function toggle() {
	if (areas.length) {
		areas.forEach(area => {
			shown ? area.hide() : area.show();
		});
		shown = !shown;
	} else {
		alert('Редактируемые области не определены на этой странице');
	}
}
