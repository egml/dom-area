import Name from '@egml/utils/Name.js';
import Widget from '@egml/utils/Widget.js';

var name = new Name(['dom', 'area']);

export default function DomArea() {
	Widget.call(this, ...arguments);
}
Object.defineProperty(DomArea, 'name', {
	value: new Name('DomArea', false),
});

DomArea.prototype = Object.create(Widget.prototype);
Object.defineProperty(DomArea.prototype, 'constructor', {
	value: DomArea,
	enumerable: false,
	writable: true,
});

DomArea.prototype.name = name;
DomArea.prototype.selector = null;
DomArea.prototype.url = null;
// Интервал обновления позиции и размера области
DomArea.prototype.repositionInterval = 1000;
DomArea.prototype.title = null;

DomArea.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	this.saveInstanceOf(DomArea);
};

DomArea.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);
	if (!this.dom.self) {
		throw new Error(`Не найдены элементы в DOM для виджета типа "${this.constructor.name}" с ID "${this.id[this.constructor.name.camel()]}"`);
	}
	this.setDatasetIdOf(DomArea);
	
	// #TODO: Добавить индикатор области при выводе ошибки, чтобы понимать с какой областью проблема
	if (!this.selector) {
		return console.error('DomArea: не определен селектор');
	}
	if (!this.url) {
		return console.error('DomArea: не определен URL');
	}
	if (!this.dom.target) {
		this.dom.target = document.querySelector(this.selector);
	}
	if (!this.dom.target) {
		return console.error('DomArea: не найден целевой элемент в DOM');
	}
	 
	// HTML
	var templateData = {
		scale: 16 / this.mountPointFontSize,
		hasTitle: this.title ? true : false,
		title: this.title,
	};
	var fragment = document.createRange().createContextualFragment(template(templateData));
	this.dom.self = fragment.firstElementChild;
	document.body.appendChild(fragment);
	 
	this.dom.self.addEventListener('click', function() {
		config.global.modal.show({
			url: options.url,
			callback: function() {
				panel.hide();
				toggle();
			},
		});
	});
	 
	// Подстройка позиции и размеров области
	this.offset = {
		left: 0,
		top: 0,
		right: 0,
		bottom: 0,
	};
	if (options.offset) {
		let matches = options.offset.match(/(-?\d+)(?: (-?\d+)(?: (-?\d+)(?: (-?\d+))?)?)?/);
		let o1 = matches[1];
		let o2 = matches[2];
		let o3 = matches[3];
		let o4 = matches[4];
		if (o1 && !o2 && !o3 && !o4) {
			this.offset.left =
			this.offset.top =
			this.offset.right =
			this.offset.bottom = Number(o1);
			// Можно еще так, но это только для целых чисел
			// this.offset.bottom = o1>>0;
		} else if (o1 && o2 && !o3 && !o4) {
			this.offset.top =
			this.offset.bottom = Number(o1);
			this.offset.left =
			this.offset.right = Number(o2);
		} else if (o1 && o2 && o3 && !o4) {
			this.offset.top = Number(o1);
			this.offset.left =
			this.offset.right = Number(o2);
			this.offset.bottom = Number(o3);
		} else if (o1 && o2 && o3 && o4) {
			this.offset.top = Number(o1);
			this.offset.right = Number(o2);
			this.offset.bottom = Number(o3);
			this.offset.left = Number(o4);
		}
	}
	 
	// Регистрация созданной области
	areas.push(this);
};